import click
import questionary
import csv
from pprint import pprint
from prompt_toolkit import print_formatted_text
from prompt_toolkit.formatted_text import FormattedText
from random import shuffle

col_names = ['Muskel', 'Ursprung', 'Ansatz', 'Innervation']
lookup = {name: i for i, name in enumerate(col_names)}

quiz_set = {name: list() for name in col_names}
answer_set = {}

question_count = 0
correct_count = 0


class bcolors:
    OKGREEN = '#33FF33'
    WARNING = '#FF3333'
    SCORE = '#FFFF33'


def load(region):
    with open(f'lib/{region}.csv', encoding='utf-8') as f:
        table = csv.reader(f, delimiter=';')
        for row in table:
            add_row_to_set(row)


def add_row_to_set(row):
    answer_set[row[0]] = row[1:]
    for name, i in lookup.items():
        quiz_set[name].append(row[i])


def answer(muscle, aspect):
    return answer_set[muscle][lookup[aspect]-1]


def score():
    return f'{correct_count} / {question_count}  =  {correct_count * 100 / question_count}%'


def print_response(result, expected):
    global question_count
    global correct_count
    question_count += 1
    if result == expected:
        correct_count += 1
        response = FormattedText([
            (bcolors.OKGREEN, "True"),
            ('', '\t\t\t'),
            (bcolors.SCORE, score())
        ])
    else:
        response = FormattedText([
            (bcolors.WARNING, "False"),
            (bcolors.OKGREEN, f" -> {expected}"),
            ('', '\t\t\t'),
            (bcolors.SCORE, score())
        ])
    print_formatted_text(response)
    print('\n')


def ask(muscle, aspect):
    shuffle(quiz_set[aspect])
    result = questionary.select(
        f"{aspect} des {muscle}?",
        choices=set(quiz_set[aspect])
    ).ask()  # returns value of selection
    print_response(result, answer(muscle, aspect))


@click.command()
@click.argument('region')
def main(region):
    load(region)
    for mus in quiz_set['Muskel']:
        ask(mus, 'Ursprung')
        ask(mus, 'Ansatz')
        ask(mus, 'Innervation')
    print(f'{correct_count} / {question_count}  =  {correct_count * 100 / question_count}%')


if __name__ == "__main__":
    main()
