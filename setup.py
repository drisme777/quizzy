from setuptools import setup

setup(name='quizzy',
      version='0.1',
      description='Medical Facts Trainer',
      url='https://gitlab.com/drisme777/quizzy.git',
      author='Marcel Francis Gellesch',
      author_email='drisme777@gmail.com',
      license='MIT',
      packages=['quizzy'],
      zip_safe=False,
      install_requires=[
        'Click',
      ],
      entry_points={
        'console_scripts':
          ['quizzy=quizzy.main:main'],
      }
)